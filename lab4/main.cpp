#include "keyboard.hpp"

int main() 
{
    setlocale(LC_ALL, "Rus");

    WorkFlow workFlow;
    Keyboard keyboard = *new Keyboard(workFlow);
    
    keyboard.setKey("x", []() { std::cout << "The action of the key <x> is completed" << std::endl; }, []() { std::cout << "The action of the key <x> is canceled" << std::endl; });
    keyboard.setKey("1", []() { std::cout << "The action of the key <1> is completed" << std::endl; }, []() { std::cout << "The action of the key <1> is canceled" << std::endl; });
    keyboard.setKey("Ctrl+c", []() { std::cout << "The action of the key <Ctrl+c> is completed" << std::endl; }, []() { std::cout << "The action of the key <Ctrl+c> is canceled" << std::endl; });

    keyboard.pressKey("x");
    keyboard.pressKey("1");
    keyboard.pressKey("Ctrl+c");
    keyboard.pressKey("y");
    keyboard.undo();
    keyboard.undo();
    
    //Переназначение существующей клавиши
    keyboard.setKey("1", []() { std::cout << "НОВОЕ действие 1" << std::endl; }, []() { std::cout << "НОВАЯ отмена 1" << std::endl; });
    keyboard.pressKey("1");
    keyboard.undo();

    //Еще одно переназначение клавиши 1
    keyboard.setKey("1", []() { std::cout << "ОЧЕНЬ НОВОЕ действие 1" << std::endl; }, []() { std::cout << "ОЧЕНЬ НОВАЯ отмена 1" << std::endl; });
    keyboard.pressKey("1");
    keyboard.undo();

    return 0;
}
