#include "FileUserManager.hpp"
#include "FileUserRepository.hpp"
#include <iostream>

int main()
{
	ul::FileUserManager manager;
	ul::FileUserRepository repository;

	std::string userName = "Ulyana";

	int command = 1;

	ul::User user = repository.checkLogin();

	if (!user.login.empty())
	{
		manager.signIn(user);
		std::cout << "You are successfully autorised\nEnter command:\n0 - log out\n1 - log in with another account\n2 - add new user" << std::endl;
	}

	std::string login, password, nickname;

	while (command != 0)
	{
		if (!manager.isAuthorized())
		{
			std::cout << "Enter command:\n0 - log out\n1 - autorization with login and password\n2 - sign up" << std::endl;
		}

		std::cin >> command;

		switch (command)
		{
		case 1:
			std::cout << "Enter login:" << std::endl;
			std::cin >> login;
			std::cout << "Enter password:" << std::endl;
			std::cin >> password;

			user = repository.getByLogin(login);

			if (!user.login.empty() && user.password == password)
			{
				manager.signIn(user);
				std::cout << "You are successfully autorised\nEnter command:\n0 - log out\n1 - log in with another account\n2 - add new user" << std::endl;
			}

			else
			{
				std::cout << "Wrong login or password!" << std::endl;
			}
			break;

		case 2:
			std::cout << "Enter login:" << std::endl;
			std::cin >> login;
			std::cout << "Enter password:" << std::endl;
			std::cin >> password;
			std::cout << "Enter nickname:" << std::endl;
			std::cin >> nickname;

			manager.signUp(nickname, login, password);
			std::cout << "You are succesfully registrated\nEnter command:\n0 - log out\n1 - log in with another account\n2 - add a new user" << std::endl;
			break;

		default:
			break;
		}
	}
	return 0;
}