#pragma once
#include "IUserRepository.hpp"

namespace ul
{
	class FileUserRepository : ul::IUserRepository
	{
	public:
		FileUserRepository();

		virtual ul::User getById(int id) override;
		virtual ul::User getByLogin(const std::string& login) override;

		virtual std::vector<ul::User> Get() const override;
		virtual void add(ul::User item) override;
		virtual void remove(ul::User item) override;
		virtual void update() override;

		virtual ul::User checkLogin();
		virtual int createNewId();

	protected:
		std::vector<ul::User> _users;
	};
}