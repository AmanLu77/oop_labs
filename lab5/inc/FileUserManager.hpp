#pragma once
#include "IUserManager.hpp"
#include "FileUserRepository.hpp"

namespace ul
{
	class FileUserManager : ul::IUserManager
	{
	public:
		FileUserManager();

		virtual void signIn(ul::User user) override;
		virtual void signOut() override;
		virtual bool isAuthorized() override;

		virtual bool isRegistered(ul::User user) override;
		virtual void signUp(std::string name, std::string login, std::string password) override;

	private:
		ul::User _currentUser;
		ul::FileUserRepository _userRepository;
	};
}