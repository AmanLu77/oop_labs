#pragma once
#include <string>

namespace ul
{
	class User
	{
	public:
		int id;
		std::string name;
		std::string login;
		std::string password;
	};
}
