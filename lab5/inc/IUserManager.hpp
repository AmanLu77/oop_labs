#pragma once
#include "User.hpp"
#include <string>

namespace ul
{
	class IUserManager
	{
	public:
		virtual void signIn(ul::User user) = 0;
		virtual void signOut() = 0;
		virtual bool isAuthorized() = 0;

		virtual bool isRegistered(ul::User user) = 0;
		virtual void signUp(std::string name, std::string login, std::string password) = 0;
	};
}