#pragma once

#include "User.hpp"
#include "IDataRepository.hpp"

namespace ul
{
	class IUserRepository : ul::IDataRepository<ul::User>
	{
	public:
		virtual ul::User getById(int id) = 0;
		virtual ul::User getByLogin(const std::string& name) = 0;
	};
}