#include "FileUserManager.hpp"

#include <iostream>
#include <fstream>

namespace ul
{
	FileUserManager::FileUserManager() {}

	void FileUserManager::signIn(ul::User user)
	{
		_currentUser = user;
		std::ofstream out("lastAuthorized.txt");

		if (out.is_open())
		{
			out << user.id << " " << user.name << " " << user.login << " " << user.password;
		}

		out.close();

		std::cout << "Autorized" << std::endl;
	}

	void FileUserManager::signOut()
	{
		_currentUser = ul::User();
		std::ofstream out("lastAuthorized.txt");

		if (out.is_open())
		{
			out << "";
		}

		out.close();

		std::cout << "Signed out" << std::endl;
	}

	bool FileUserManager::isAuthorized()
	{
		return !_currentUser.login.empty();
	}
	
	bool FileUserManager::isRegistered(ul::User user)
	{
		ul::User userN;
		std::ifstream in("users.txt");
		std::vector<ul::User> users;
	
		while (in >> userN.id >> userN.name >> userN.login >> userN.password)
		{
			users.push_back(userN);
		}

		in.close();

		for (ul::User u : users)
		{
			if (u.id == user.id || u.login == user.login)
			{
				return true;
			}
		}

		return false;
	}

	void FileUserManager::signUp(std::string name, std::string login, std::string password)
	{
		ul::User user = _userRepository.getByLogin(login);

		if (user.login.empty())
		{
			ul::User newUser;
			newUser.id = _userRepository.createNewId();
			newUser.name = name;
			newUser.login = login;
			newUser.password = password;

			_userRepository.add(newUser);
			signIn(newUser);
		}
		else
		{
			std::cout << "This login is already taken" << std::endl;
		}
	}
}