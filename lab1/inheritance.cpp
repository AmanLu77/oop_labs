#include <iostream>
#include <vector>

class Derivative
{
public:
	Derivative(double x, double step) 
	{
		_x = x;
		_step = step;
		std::cout << "constructor (base)\n";
	}

	~Derivative() 
	{
		std::cout << "destructor (base)\n";
	}
	
	double function(double y) //f(y) = y^3
	{	
		return pow(y, 3);
	}
	virtual void calc() = 0;

	double value() const {
		return _derivative;
	}

protected:
	double _derivative = 0;
	double _x;
	double _step;
};

//��������� -  ����� �����������
class Left : public Derivative
{
public:
	Left(double x, double step) : Derivative(x, step)
	{
		std::cout << "constructor (LEFT)\n";
	}
	~Left()
	{
		std::cout << "destructor (LEFT)\n";
	}
	void calc() override
	{
		_derivative = (function(_x) - function(_x - _step)) / _step;
	}
	
};

//��������� -  ������ �����������
class Right : public Derivative
{
public:
	Right(double x, double step) : Derivative(x, step)
	{
		std::cout << "constructor (RIGHT)\n";
	}
	~Right()
	{
		std::cout << "destructor (RIGHT)\n";
	}
	void calc() override
	{
		_derivative = (function(_x + _step) - function(_x)) / _step;
	}
};

//��������� -  ����������� �����������
class Center : public Derivative
{
public:
	Center(double x, double step) : Derivative(x, step)
	{
		std::cout << "constructor (CENTER)\n";
	}
	~Center()
	{
		std::cout << "destructor (CENTER)\n";
	}
	void calc() override
	{
		_derivative = (function(_x + _step) - function(_x - _step)) / (2 * _step);
	}
};

void main()
{
	std::vector<Derivative*> derivatives;

	derivatives.push_back(new Left(3, 0.1));
	derivatives.push_back(new Right(3, 0.1));
	derivatives.push_back(new Center(3, 0.1));
	
	for (const auto& derivative : derivatives)
	{
		derivative->calc();
		std::cout << derivative->value() << std::endl;
	}

	for (auto& derivative : derivatives)
		delete derivative;
	derivatives.clear();
}