#include <iostream>

namespace ul
{
    template <typename T>
    class Array3d
    {
    public:
        Array3d(int sl, int str, int col) : _sl(sl), _str(str), _col(col) { _array = new T[sl * str * col]; }

        Array3d(const Array3d& array3D) = delete;
        Array3d& operator=(const Array3d& array3D) = delete;

        ~Array3d() { delete[] _array; }


        void setValue(int i, int j, int k, const T& value) { _array[k * (_sl * _str) + i * _sl + j] = value; }

        T getValue(int i, int j, int k) { return _array[k * (_sl * _str) + i * _sl + j]; }


        const T& operator()(int i, int j, int k) const { return _array[k * (_sl * _str) + i * _sl + j]; }

        T& operator()(int i, int j, int k) { return _array[k * (_sl * _str) + i * _sl + j]; }


        void fill_Ones() { Fill(1); }

        void fill_Zeros() { Fill(0); }

        void fill_Num(int number) { Fill(number); }


        void Print()
        {
            for (int i = 0; i < _sl; i++) {  // ����
                for (int j = 0; j < _str; j++) {  // ������
                    for (int k = 0; k < _col; k++) {  // �������
                        std::cout << _array[(i, j, k)] << " ";
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            }
        }

    private:
        T* _array;
        int _sl, _str, _col; // ���� ������ �������

        void Fill(int num)
        {
            for (int i = 0; i < _sl; i++) {
                for (int j = 0; j < _str; j++) {
                    for (int k = 0; k < _col; k++) {
                        _array[(i, j, k)] = num;
                    }
                }
            }
        }
    };
}

int main()
{
    ul::Array3d<int> array3D(3, 3, 3);

    array3D.setValue(2, 1, 1, 999);
    std::cout << array3D.getValue(2, 1, 1) << std::endl;

    array3D.fill_Num(5);
    array3D.Print();
    
    array3D.fill_Zeros();
    array3D.Print();

    array3D.fill_Ones();
    array3D.Print();
   

    return 0;
}